import * as React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import NavigationMenu from './all pages/pagesApp/Main/index';
import Login from './all pages/Initial/index';
import {NavigationActions, StackActions} from 'react-navigation';
import Splash from './all pages/Splash/index';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoginChecked: null,
      view: <Splash/>,
    };
  }


  componentDidMount() {
    setTimeout(() => {
      this.isLoginCheck()
    }, 2000);
  }

  isLoginCheck() {
    AsyncStorage.getItem('isLoginChecked').then((filter) => {
      if (filter != null) {
        // console.log('returned filter:', filter);
        if (filter !== "false") {
          this.setState({
            view: <NavigationMenu/>,
          });
        } else {
          this.setState({
            view: <Login/>,
          });
        }
        this.setState({isLoginChecked: filter == 'false' ? false : true});
      } else {
        this.setState({
          view: <Login/>,
        });
        console.log('error');
      }
    });
  }

  // splash() {
  //   return <Splash/>;
  // }

  render() {
     return (
        this.state.view
      );
  }

  // let isLoginChecked = false;
  // while (this.state.isLoginChecked !== undefined) {
  //   isLoginChecked = this.isLoginCheck();
  // }

  //   if (isLoginChecked === null) {
  //     return this.splash();
  //   } else if (isLoginChecked === true) {
  //     return (
  //       <NavigationMenu/>
  //     );
  //   } else {
  //     return (<Login/>);
  //   }
  // }
}

export default App;
