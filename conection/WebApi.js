import React from 'react';
import OrderStatic from '../staticClass/OrderStatic';
//for restAPI
class WebApi extends React.Component {

  static URL = {
    base: 'https://api.sipars.ir/v1/',
  };

  constructor(url, body = {}) {
    super();
    this.url = url || {};
    this.body = body || {};
    if (this.body.length > 0) {
      return this._PostFetch(this.body);
    } else {
      return this._fetch();
    }
  }


  getUrl() {
    return WebApi.URL.base + this.url;
  }

  async _fetch() {
    try {
      let response = await fetch(this.getUrl(),{
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'cookie': "__RequestVerificationToken=6QEP4y8T4VTn7LNVRv5IvAPjv6FtoyTeo_5DrDd_gTXwzFJLPoHOM8CehW1xgBtNmgNQ2hTz9tTp3qKKupw4SLG6J3OHaiJ850MASvaMNq81; my-very-own-cookie-name=SDBXJxsd3fVjXCWL_BMVT4jPK2nnp4V2xhkiofl11h5E8B-h6z-zA88tHAM-kWrTZ9mRokeQ5SQDwzQrI_9Jh2vFJnuIRppMHAwPWuw1RqT2xaF4gv5Bg_fN-Gr0kvex7KlUkNNBFuwV1O8BMkg4oMMADGNdiR8UGfyT4MdCefVmCNFR4Gz9lp55DMfU0E66PjRC2K6t7hL9CbpFZ5oi4NI6JyLHGamt89RLLMyMB-122BUn1iSTMxwdvPijyucT_MHGr4dw_FbVBeWlmHFkCY8m7Pa_H0-vR2VF7bn7orT56JBSnpHoN6zLSKiYEc1RlH6-U0GEVXiyZ3wVEc9bsaDAW-ZJtNRYKvdji6xo0lqTTGRlqnYF-q81edMgq_fbLNRGN6NbukNf-fdCs6RhW82cqDt6yg682aBhi-OHKi8xh_2jt5vUo98DS4iBmcUBPvd4u98KoDB3HC_SyAkhhi_qLJe2anYEHWbXgcqdoXI",
        },
      });
      let responseJson = await response.json();
      return responseJson;
    } catch (error) {
      console.log(error);
      alert(error);
    }
  }


  async _PostFetch(body) {
    try {
      // console.log('before');
      // console.log(OrderStatic.cookie);
      const response = await fetch(this.getUrl(), {   //'http://api.sipars.ir/v1/Account/Login'
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'cookie': "__RequestVerificationToken=6QEP4y8T4VTn7LNVRv5IvAPjv6FtoyTeo_5DrDd_gTXwzFJLPoHOM8CehW1xgBtNmgNQ2hTz9tTp3qKKupw4SLG6J3OHaiJ850MASvaMNq81; my-very-own-cookie-name=SDBXJxsd3fVjXCWL_BMVT4jPK2nnp4V2xhkiofl11h5E8B-h6z-zA88tHAM-kWrTZ9mRokeQ5SQDwzQrI_9Jh2vFJnuIRppMHAwPWuw1RqT2xaF4gv5Bg_fN-Gr0kvex7KlUkNNBFuwV1O8BMkg4oMMADGNdiR8UGfyT4MdCefVmCNFR4Gz9lp55DMfU0E66PjRC2K6t7hL9CbpFZ5oi4NI6JyLHGamt89RLLMyMB-122BUn1iSTMxwdvPijyucT_MHGr4dw_FbVBeWlmHFkCY8m7Pa_H0-vR2VF7bn7orT56JBSnpHoN6zLSKiYEc1RlH6-U0GEVXiyZ3wVEc9bsaDAW-ZJtNRYKvdji6xo0lqTTGRlqnYF-q81edMgq_fbLNRGN6NbukNf-fdCs6RhW82cqDt6yg682aBhi-OHKi8xh_2jt5vUo98DS4iBmcUBPvd4u98KoDB3HC_SyAkhhi_qLJe2anYEHWbXgcqdoXI",
          },
          body: body,
        })
      ;
      // OrderStatic.cookie = response.headers.get('set-cookie');
      // console.log(OrderStatic.cookie);
      return await response.json();
    } catch (error) {
      console.log(error);
      alert(error);
    }
  }


  static filterItems(items, parentId, level, isEnable) {
    return items.filter(item => item.parentId === parentId && item.level === level && item.isEnable === isEnable);
  }
}

export default WebApi;
