import EStyleSheet from 'react-native-extended-stylesheet';

export default EStyleSheet.create({
  splash_text: {
    fontSize: 40,
    color: "white",
    fontFamily: 'IRANSansMobile_Bold'
  },
  tabIcon: {
    fontFamily: 'IRANSansMobile_Bold'
  },

});
