import React, {Component} from 'react';
import chooseAddress from './chooseAddress';
import chooseServant from './chooseServant';
import chooseType from './chooseType';
import rateSetting from './rateSetting';
import newAdress from './newAdress';
import timeSelection from './timeSelection';
import orderSummery from './orderSummery';
import chooseService from './chooseService';
import viewTariff from './viewTariff';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';


const App = createStackNavigator({
    timeSelection: {screen: timeSelection},
    viewTariff: {screen: viewTariff},
    chooseType: {screen: chooseType},
    chooseServant: {screen: chooseServant},
    chooseAddress: {screen: chooseAddress},
    newAdress: {screen: newAdress},
    // typeService: {screen: typeService},
    rateSetting: {screen: rateSetting},
    orderSummery: {screen: orderSummery},
    chooseService: {screen: chooseService},
  },
  {
    initialRouteName: 'newAdress',
  });

const AppContainer = createAppContainer(App);
export default AppContainer;

