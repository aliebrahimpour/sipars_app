import React,{useState} from 'react';
import {FlatList, Image, Modal, Picker, ScrollView, StyleSheet, Text, TextInput} from 'react-native';
import {Button, Item} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,
  Form,
  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
  Input,
} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';
import Header from '../../../components/Header';
import BodySelectService from '../../../components/BodySelectService';
import ChildItemBody from '../Home/home';
import WebApi from '../../../conection/WebApi';
import RadioButton from '../../../components/RadioButtons';
import OrderStatic from '../../../staticClass/OrderStatic';
import {
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';

export default class chooseService extends React.Component {


  static navigationOptions = ({navigation}) => ({
    headerTitle:
      <Header text={'انتخاب خدمت '}/>
    , headerLeft: null,
    headerStyle: {
      backgroundColor: '#785599',
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      data: this.props.navigation.getParam('data'),
      childItems: this.props.navigation.getParam('childItems'),
      autoFocusSearch: this.props.navigation.getParam('autoFocusSearch'),
      modalVisible: false,
      listCity: '',
      listLocation: [],
      selected: 0,   //location
      modalLocationVisible: false,
      checked: false,
    };
  }

  toggleModal(visible) {
    this.setState({modalVisible: visible});
  }

  // toggleModalLocation(visible) {
  //   this.setState({modalLocationVisible: visible});
  // }

  async componentDidMount(): Promise<void> {
    let responseJson = await new WebApi('base/cityList');
    this.setState({listCity: responseJson.items});
    this.getListLocationByCity(2)
  }

  async getListLocationByCity(cityId) {
    try {
      let response = await fetch('http://api.sipars.ir/PrivateTrain/ServiceLocation/ListLocationGetByCityId', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          'CityId': cityId,
        }),
      });
      let responseJson = await response.json();
      console.log('location');
      this.setState({
        listLocation: responseJson.list,
      });
      console.log(this.state.listLocation);
      // this.toggleModalLocation(true);
      return responseJson;

    } catch (error) {
      alert(error);
      console.error(error);
    }
  }

  //todo sreach
  // $changeSearch = function () {
  //
  //   const list = $scope.data.serviceList
  //
  //   if ($scope.data.searchList.length == 0) {
  //
  //     const tempSearchList = []
  //
  //     const add = function (service, title) {
  //       let callAdd = false
  //       list.filter(s => s.parentId == service.id).forEach(s => {
  //         callAdd = 1
  //         add(s, title + " " + s.title)
  //       })
  //
  //       if (!callAdd) {
  //         tempSearchList.push({
  //           id: service.id,
  //           title: title + " " + service.title,
  //         })
  //       }
  //     }
  //
  //     list.filter(s => s.parentId == 0).forEach(s => {
  //       add(s, s.title)
  //     })
  //     $scope.data.searchList = tempSearchList
  //   }
  //
  //   const text = $scope.model.search
  //   const textList = text.split(' ')
  //   const searchList = $scope.data.searchList
  //   const result = searchList.filter(s => textList.find(t => s.title.includes(t)))
  //   $scope.model.searchServiceList = list.filter(s => result.find(r => r.id == s.id)).slice(0, 25)
  //   $scope.model.searchServiceTitles = {}
  //   $scope.model.searchServiceList.forEach(s => {
  //     const parent = list.find(service => service.id == s.parentId) || {}
  //     const parent2 = list.find(service => service.id == parent.parentId) || {}
  //     $scope.model.searchServiceTitles[s.id] = parent2.title + ' ' + parent.title + ' ' + s.title
  //   })
  //   console.log(result)
  // }

  onValueChange(value: string) {
    this.setState({
      selected: value,
    });
  }

  // renderItemLocation() {
  //   // const [selectedValue, setSelectedValue] = useState("java");
  //   // return (
  //   //   <Picker
  //   //     style={{ height: 50, width: 150 }}
  //   //     onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
  //   //   >
  //   //     <Picker.Item label="Java" value="java" />
  //   //     <Picker.Item label="JavaScript" value="js" />
  //   //   </Picker>
  //   //   );
  //
  //   // return (
  //   //   <FlatList
  //   //     data={this.state.listLocation}
  //   //     renderItem={({item}) =>
  //   //       <ListItem style={{backgroundColor: this.state.checked ? '#6e3b6e' : '#f9c2ff'}}>
  //   //         <TouchableOpacity
  //   //           keyExtractor={item.id}
  //   //           onPress={() => this.setState({checked: !this.state.checked})}
  //   //           style={[
  //   //             styles.item,
  //   //             {flexDirection: 'row'},
  //   //           ]}>
  //   //           <CheckBox
  //   //             value={this.state.checked}
  //   //             checked={this.state.checked} color="green"/>
  //   //           <Text style={[styles.text, {marginLeft: 5}]}>{item.Name}</Text>
  //   //         </TouchableOpacity>
  //   //       </ListItem>
  //   //     }
  //   //   />
  //   // );
  //
  //
  //
  //
  //
  //   // const selected = 1;
  //   // const [selected, setSelected] = React.useState(new Map());
  //
  //   // const onSelect = React.useCallback(
  //   //   id => {
  //   //     const newSelected = new Map(selected);
  //   //     newSelected.set(id, !selected.get(id));
  //   //
  //   //     setSelected(newSelected);
  //   //   },
  //   //   [selected],
  //   // );
  //
  //
  //   // return this.state.listLocation.map((v) => {
  //   //   return ;
  //   // });
  // }
  //
  // onChangeTextType(Text){
  //   return console.log(Text);
  // }
  render() {
    // console.log(this.state.data);

    return (
      <Container style={{backgroundColor: 'rgb(226,224,225)'}}>
        <View style={styles.container}>
          <Modal animationType={'slide'} transparent={true}
                 visible={this.state.modalVisible}
                 onRequestClose={() => {
                   console.log('Modal has been closed.');
                 }}>
            <View style={styles.modal}>
              <Text style={styles.text}>لطفا شهر مورد نظر خود را انتخاب کنید:</Text>

              <RadioButton style={{height: 50, width: 10, borderWidth: 5}} options={this.state.listCity}/>
              <Button onPress={() => {
                this.toggleModal(!this.state.modalVisible);
              }}
                      style={{borderRadius: 5}} rounded success>
                <Text style={{
                  paddingLeft: 40,
                  fontSize: 14,
                  fontFamily: 'IRANSansMobile_Bold',
                  paddingRight: 40,
                }}>اعمال</Text>
              </Button>

            </View>
          </Modal>
        </View>

        {/*<View style={styles.container}>*/}
          {/*<Modal animationType={'slide'} transparent={true}*/}
                 {/*visible={this.state.modalLocationVisible}*/}
                 {/*onRequestClose={() => {*/}
                   {/*console.log('Modal has been closed.');*/}
                 {/*}}>*/}
            {/*<View style={styles.modalLocation}>*/}
              {/*<Text style={styles.text}>لطفا منطقه مورد نظر خود را انتخاب کنید:</Text>*/}
              {/*{this.renderItemLocation()}*/}


              {/*<Button*/}
                {/*onPress={() => {*/}
                  {/*this.toggleModalLocation(!this.state.modalLocationVisible);*/}
                {/*}}*/}
                {/*style={{borderRadius: 5, marginLeft: 80, width: '50%', justifyContent: 'center'}} rounded success>*/}
                {/*<Text style={{*/}
                  {/*fontSize: 14,*/}
                  {/*fontFamily: 'IRANSansMobile_Bold',*/}
                {/*}}>اعمال</Text>*/}
              {/*</Button>*/}

            {/*</View>*/}
          {/*</Modal>*/}
        {/*</View>*/}


        <View style={{justifyContent: 'center', marginTop: 10, marginLeft: 5, marginRight: 5}}>

          <Item style={{height: 50, width: 350, flexDirection: 'row', borderColor: 'gray', backgroundColor: 'white'}}
                rounded>
            <Icon name="location" size={40}/>
            <Input style={{color: 'black', textAlign: 'center', fontFamily: 'IRANSansMobile', fontSize: 16}}
                   value="مشهد"
                   onFocus={() =>
                     this.toggleModal(true)
                   }
                   placeholderTextColor="gray"
                   placeholder="شهر"/>

            <View style={{borderWidth: 1, height: 40, borderColor: 'gray'}}/>



            <Picker
              style={{height: 50, width: 200,}}
              itemStyle={{color: 'black', textAlign: 'center', fontFamily: 'IRANSansMobile', fontSize: 16}}
              onValueChange={this.onValueChange.bind(this)}
              selectedValue={this.state.selected}
            >
              <Picker.Item value='0' key={0} label='لطفا انتخاب کنید'/>
              {this.state.listLocation.map(row =>(
                <Picker.Item label={row.Name} key={row.Id} value={row.Id}/>
              ))}
            </Picker>


            {/*<Input style={{color: 'black', textAlign: 'center', fontFamily: 'IRANSansMobile', fontSize: 16}}*/}
                   {/*onFocus={() =>*/}
                     {/*this.getListLocationByCity(2)*/}
                   {/*}*/}
                   {/*placeholderTextColor="gray"*/}
                   {/*placeholder="منطقه"/>*/}

          </Item>

          <Item style={{height: 50, width: 350, borderColor: 'gray', backgroundColor: 'white', marginTop: 10}}
                rounded>
            <Input
              autoFocus={this.state.autoFocusSearch}
              style={{color: 'black', textAlign: 'center', fontFamily: 'IRANSansMobile', fontSize: 16}}
              placeholderTextColor="gray"
              onChangeText={text => this.onChangeTextType(text)}
              placeholder="دسته بندی مورد نظر خود را بنویسید"/>
            <Icon name="search" size={40}/>
          </Item>

          <BodySelectService navigation={this.props.navigation} childItems={this.state.childItems}
                             data={this.state.data}/>

        </View>

      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#ede3f2',
    padding: 0,
  },
  modal: {
    borderWidth: 3,
    height: '60%',
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255,255,255,1)',
    marginTop: '30%',
    marginLeft: '10%',
    marginRight: '10%',
  },
  modalLocation: {
    marginTop: '5%',
    marginLeft: '5%',
    marginRight: '5%',
    width: '90%',
    height: '95%',
    backgroundColor: 'rgba(255,255,255,1)',
  },
  text: {
    color: '#3f2949',
    marginTop: 10,
    fontSize: 12,
    fontFamily: 'IRANSansMobile_Bold',
  },
});
