import React from 'react';
import {Image, Text, TouchableHighlight, Modal, StyleSheet, ActivityIndicator} from 'react-native';
import {Button} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,
  Card,
  Radio,
  DatePicker,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
  Picker,
  Separator,
} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';
import Header from '../../../components/Header';
import PersianCalendarPicker from 'react-native-persian-calendar-picker';
import DateTimePicker from '@react-native-community/datetimepicker';


export default class timeSelection extends React.Component {
  state = {
    language: 1,
  };

  static navigationOptions = ({navigation}) => ({
    headerTitle:
      <Header text={'انتخاب زمان'}/>
    , headerLeft: null,
    headerStyle: {
      backgroundColor: '#785599',
    },

  });
  constructor(props){
    super(props);
    this.state = {
      modalDateVisible: false,
      modalTimeVisible: false,
      selectedStartDate: null,
      date: new Date('2020-06-12T14:42:42'),
      mode: 'time',
      show: false,
    };
    this.onDateChange = this.onDateChange.bind(this);
  }
  onDateChange(date) {
    this.setState({ selectedStartDate: date });
  }
  setModalDateVisible(visible) {
    this.setState({modalDateVisible: visible});
  }
  // setModalTimeVisible(visible) {
  //   this.setState({modalTimeVisible: visible});
  // }

  timepicker = () => {
    this.show('time');
  }
  show = mode => {
    this.setState({
      show: true,
      mode,
    });
  }
  setDate = (event, date) => {
    date = date || this.state.date;

    this.setState({
      show: Platform.OS === 'ios' ? true : false,
      date,
    });
  }

  render() {
    const { selectedStartDate } = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : '';
    const { show, date, mode } = this.state;

    return (
      <Container style={{backgroundColor: '#e1e1e1'}}>
        <Body>
        <View style={{alignItems: 'center'}}>
          <View style={{marginTop: 40}}>
            <Icon2 name="date" size={80}/>
          </View>


          { show && <DateTimePicker value={date}
                                    mode={mode}
                                    is24Hour={true}
                                    display="default"
                                    onChange={this.setDate} />
          }

          {/*<Modal*/}
            {/*transparent={true}*/}
            {/*animationType="slide"*/}
            {/*visible={this.state.modalTimeVisible}*/}
            {/*onRequestClose={() => {console.log('close modal')}}>*/}
                {/*<DateTimePicker value={date}*/}
                                          {/*mode={'time'}*/}
                                          {/*is24Hour={true}*/}
                                          {/*display="default"*/}
                                          {/*onChange={this.setDate} />*/}


          {/*</Modal>*/}

          <Modal
            transparent={true}
            animationType="slide"
            visible={this.state.modalDateVisible}
            onRequestClose={() => {console.log('close modal')}}>
            <View style={styles.modalBackground}>
              <View style={styles.activityIndicatorWrapper}>
                <PersianCalendarPicker
                  onDateChange={this.onDateChange}
                />
                <TouchableHighlight
                  onPress={() => {
                    this.setModalDateVisible(!this.state.modalDateVisible);
                  }}>
                  <Text style={{fontFamily: 'IRANSansMobile_Bold'}}>بستن</Text>

                </TouchableHighlight>
              </View>
            </View>
          </Modal>
          <View style={{marginTop: 20, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontSize: 14, fontFamily: 'IRANSansMobile_Bold'}}>لطفا زمان خود مورد نظر خود را انتخاب
              کنید:</Text>
            <Text style={{fontSize: 10, fontFamily: 'IRANSansMobile'}}>اگر زمان ارئه خدمت قابل جابجایی است بهتر است
              تلفنی
              با خدمتیار هماهنگ شود.‍</Text>
          </View>

          <View style={{flexDirection:"row",justifyContent: 'center',marginTop:30,width: 250, height: 40}}>
            <Button transparent style={{borderWidth:3,width:60,justifyContent: 'center',borderRadius: 28,}}
              onPress={() => {
                this.setModalDateVisible(true);
              }}>
              <Text style={{fontFamily: 'IRANSansMobile_Bold'}}>روز‍</Text>
            </Button>
            <Button transparent style={{borderWidth:3,marginLeft:10,width:60,justifyContent: 'center',borderRadius: 28,}}
                    onPress={this.timepicker}>
              <Text style={{fontFamily: 'IRANSansMobile_Bold'}}>ساعت</Text>
            </Button>
          </View>
          <View style={{flexDirection: 'row', marginTop: 120}}>
            <Radio selected={false}/>
            <Text style={{fontSize: 14, fontFamily: 'IRANSansMobile_Bold'}}>با خدمتیار هماهنگ میکنم</Text>
          </View>
        </View>
        </Body>
        <Footer>
          <FooterTab>
            <Button
              onPress={() => this.props.navigation.navigate('chooseAddress')}
              style={{backgroundColor: 'rgb(61,166,73)'}} full>
              <Text
                style={{fontSize: 16, color: 'white', fontFamily: 'IRANSansMobile_Bold'}}>ادامه</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});
