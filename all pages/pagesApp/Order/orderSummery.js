import React from 'react';
import {Image, Picker, Text} from 'react-native';
import {Button, Item, Thumbnail} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,

  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';
import Header from '../../../components/Header';
import WebApi from '../../../conection/WebApi';
import OrderStatic from '../../../staticClass/OrderStatic';


export default class orderSummery extends React.Component {

  static navigationOptions = ({navigation}) => ({
    headerTitle:
      <Header text={'خلاصه سفارش'}/>
    , headerLeft: null,
    headerStyle: {
      backgroundColor: '#785599',
    },
  });

  async setOrder() {
    let jsonBody = JSON.stringify({
      serviceId: OrderStatic.service.id,
      locationId: 26,
      ServiceLevelListId: OrderStatic.servant.item.serviceLevelListId,
      providerType: 'providerSelectCustomer',
      providerServiceLocationStatus: 'none',
      userDescription: '',  //todo add
      userCityId: 2,
      userCityTitle: 'مشهد',
      userAddress: 'دذرر', //todo add
      userMobile: '854',//todo add
      serviceLocationId: '8875',//todo add
      serviceProviderId: '6646',//todo add
      totalPrice: '74800',//todo add
      time: '18:00:00',//todo add
      workPriceList: '[{"workUnitId":3,"workCount":1}]',//todo add
    });
    let responseJson = await new WebApi('PrivateTraining/BuyServiceSelectProvider', jsonBody);
    console.log(responseJson);

  }

  render() {
    return (
      <Container>
        <View style={{flex: 1, alignItems: 'center', backgroundColor: 'rgb(226,224,225)'}}>
          <Card style={{width: '80%'}}>
            <CardItem>
              <Left>
                <View>
                  <Thumbnail source={require('../../../assets/images/bg.png')}/>
                  <Text style={{fontSize: 12, fontFamily: 'IRANSansMobile_Bold'}}>علی ابراهیم پور</Text>
                </View>
                <Body>
                <Text style={{fontSize: 14, fontFamily: 'IRANSansMobile_Bold'}}>آموزش علوم اول دبستان</Text>

                <View style={{marginTop: 10, flexDirection: 'row'}}>
                  <Text style={{color: '#49167f', fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>هزینه هر
                    واحد:</Text>
                  <Text style={{fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>75/000 هزار تومان</Text>
                </View>

                <View style={{marginTop: 5, flexDirection: 'row'}}>
                  <Text style={{color: '#49167f', fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>تعداد واحد:</Text>
                  <Text style={{fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>2 روز</Text>
                </View>

                <View style={{marginTop: 5, flexDirection: 'row'}}>
                  <Text style={{color: '#49167f', fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>جمع کل:</Text>
                  <Text style={{fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>75/000 هزار تومان</Text>
                </View>

                <View style={{marginTop: 5, flexDirection: 'row'}}>
                  <Text style={{color: '#e94b4a', fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>تخفیفات:</Text>
                  <Text style={{color: '#e94b4a', fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>75/000 هزار
                    تومان</Text>
                  <Text style={{
                    marginLeft: 28,
                    paddingLeft: 5,
                    paddingRight: 5,
                    backgroundColor: '#e92d2a',
                    color: 'white',
                    fontSize: 10,
                    fontFamily: 'IRANSansMobile_Bold',
                  }}>33 درصد</Text>
                </View>

                <Text
                  style={{
                    textAlign: 'left',
                    fontFamily: 'IRANSansMobile',
                  }}>--------------------------------------</Text>

                <View style={{marginTop: 5, flexDirection: 'row'}}>
                  <Text style={{color: '#46931f', fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>مبلغ نهایی:</Text>
                  <Text style={{color: '#46931f', fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>75/000 هزار
                    تومان</Text>
                </View>

                </Body>
              </Left>
            </CardItem>

          </Card>
        </View>

        <Footer style={{backgroundColor: 'rgb(226,224,225)'}}>
          <FooterTab style={{marginRight: 5, width: '48%'}}>
            <Button
              onPress={() => this.props.navigation.navigate('orderSummery')}
              style={{backgroundColor: '#851cd7', borderRadius: 5}} rounded>
              <Icon name="plus" style={{color: 'white'}} size={30}/>
              <Text style={{fontSize: 12, color: 'white', fontFamily: 'IRANSansMobile_Bold'}}>اضافه کردن خدمت
                دیگر</Text>
            </Button>
          </FooterTab>
          <FooterTab style={{width: '48%'}}>
            <Button
              onPress={() => this.setOrder()}
              success style={{borderRadius: 5}} rounded>
              <Text style={{
                paddingLeft: 40,
                fontSize: 12,
                color: 'white',
                fontFamily: 'IRANSansMobile_Bold',
                paddingRight: 40,
              }}>ثبت نهایی</Text>
            </Button>
          </FooterTab>
        </Footer>

      </Container>

    );
  }

}
