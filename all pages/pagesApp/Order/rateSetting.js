import React from 'react';
// import {Image, Text, View} from 'react-native';
import {Button, Input, Item} from 'native-base';
// import Icon from '../Account';
import {ScrollView, TextInput, Image, Text} from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import {
  Footer,
  Container,
  FooterTab,
  View,
  Accordion,
  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
} from 'native-base';
import RadioButton from '../../../components/RadioButtons';
import Header from '../../../components/Header';
import OrderStatic from '../../../staticClass/OrderStatic'
const options = [
  {
    title: 'all',
    content: 'تعداد جلسات',
  },
  {
    title: 'unknown',
    content: 'دقیقا مشخص نیست',
  },
  {
    title: 'unknown',
    content: 'دقیقا مشخص نیست',
  },
];

export default class rateSetting extends React.Component {
  constructor(props) {
    super(props);
    this.state = {text: 'Placeholder Text'};
  }

  static navigationOptions = ({navigation}) => ({
    headerTitle:
      <Header text={'مشخصات خدمت'}/>,
    headerLeft: null,
    headerStyle: {
      backgroundColor: '#785599',
    },
  });

  componentDidMount(){
    this.getServiceWorkUnitList()
  }

  async getServiceWorkUnitList() {
    // console.log(OrderStatic.serviceSelect.id)
    // console.log(OrderStatic.servant.item.serviceLevelListId)
    let body =JSON.stringify({
      ServiceId: OrderStatic.serviceSelect.id,
      LocationId: 26,
      ServiceLevelListId: OrderStatic.servant.item.serviceLevelListId,
    });
    try {
      let response = await fetch('http://api.sipars.ir/PrivateTrain/ServiceProperties/LoadServiceWorkUnit', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body:body
      });
      response = await response.json();
      // fetch('http://api.sipars.ir/v1/privateTraining/MenuList');
      console.log(response);
      this.setState({data: response.WorkUnits});

      return response;
    } catch (error) {
      alert(error);
      console.error(error);
    }
  }


  _renderHeader(item, expanded) {
    return (
      <Card style={{justifyContent: 'center', marginTop: 20, alignItems: 'center'}}>

        <View style={{
          flexDirection: 'row',
          padding: 10,
          justifyContent: 'space-between',
          alignItems: 'center',
          backgroundColor: '#A9DAD6',
        }}>
          <Left style={{flexDirection: 'row'}}>
            {expanded
              ? <CheckBox checked={true}/>
              : <CheckBox checked={false}/>}

            <Text style={{fontSize: 12, marginLeft: 20, fontFamily: 'IRANSansMobile_Bold'}}>{item.WorkUnitTitle}</Text>
          </Left>
          <Right>
            <Text style={{fontSize: 12, marginLeft: 10, fontFamily: 'IRANSansMobile_Bold'}}>{item.PriceWorkUnit}</Text>
          </Right>
        </View>
      </Card>
    );
  }

  _renderContent(item) {
    return (
      <ListItem style={{justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{fontSize: 10, marginLeft: 5, marginRight: 5, fontFamily: 'IRANSansMobile_Bold'}}>تعداد
          جلسات</Text>

        <Input  style={{color: 'black',borderWidth:1,borderRadius:5, textAlign: 'center',width:30, fontFamily: 'IRANSansMobile', fontSize: 10}}
               placeholderTextColor="gray"
               placeholder="تعداد جلسات"/>

        <Radio style={{marginLeft: 20}} selected={false}/>
        <Text style={{fontSize: 12, fontFamily: 'IRANSansMobile_Bold'}}>دقیقاً مشخص نیست</Text>
      </ListItem>
    );
  }

  render() {
    return (
      <Container>
        <Content>
          <Body>
          <ListItem style={{width: '90%'}}>
            <Accordion
              dataArray={this.state.data}
              animation={true}
              expanded={false}
              renderHeader={this._renderHeader}
              renderContent={this._renderContent}
            />
          </ListItem>


          <Card style={{width: 300}}>
            <Text style={{fontSize: 12, marginLeft: 10, fontFamily: 'IRANSansMobile_Bold'}}>توضیحات</Text>
            <Textarea style={{fontFamily: 'IRANSansMobile'}} rowSpan={5} bordered
                      placeholder="توصیحات خود را بنویسید."/>
          </Card>


          </Body>
        </Content>
        <Footer>
          <FooterTab>
            <Button
              onPress={() => this.props.navigation.navigate('timeSelection')}
              style={{flex: 1, backgroundColor: 'rgb(61,166,73)'}} full>
              <Text
                style={{fontSize: 16, color: 'white', marginLeft: 10, fontFamily: 'IRANSansMobile_Bold'}}>ادامه</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

