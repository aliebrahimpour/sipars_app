import React from 'react';
import {FlatList, Modal, Picker, StyleSheet, Text, View} from 'react-native';
// import Icon from 'react-native-vector-icons/EvilIcons';
import {Body, Button, Card, CardItem, Container, Content, Footer, FooterTab, Left, Thumbnail} from 'native-base';
import {Col, Grid, Row} from 'react-native-easy-grid';
import Icon3 from 'react-native-vector-icons/FontAwesome5';
import RadioButton from '../../../components/RadioButtons';
// import PickerChoose from '../../../components/PickerChoose';
import Header from '../../../components/Header';
import ItemMyService from '../../../components/ItemMyService';
import Servant from '../../../components/Servant';
import WebApi from '../../../conection/WebApi';
import OrderStatic from '../../../staticClass/OrderStatic';
import Loader from '../../../components/Loader';

const options = [
  {
    key: 'all',
    text: 'همه',
  },
  {
    key: 'man',
    text: 'آقا',
  },
  {
    key: 'women',
    text: 'خانم',
  },
];


export default class chooseServant extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      modalVisible: false,
      modalVisibleServant: false,
    };
  }


  toggleModal(visible) {
    this.setState({modalVisible: visible});
  }

  static navigationOptions = ({navigation}) => ({
    headerTitle:
      <Header text={'انتخاب خدمتیار  '}/>
    , headerLeft: null,
    headerStyle: {
      backgroundColor: '#785599',
    },
  });

  renderServant() {  //todo add modal
    return (
      <FlatList
        numColumns={2}
        data={this.state.data}
        selected={false}
        renderItem={(item) =>
          <Servant  navigation={this.props.navigation} item={item}/>
        }
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }

  componentDidMount() {
    this.getMenuList();
  }

  async getMenuList() {
    const serviceId = (OrderStatic.serviceSelect.id).toString();
    let jsonBody = JSON.stringify({
      "serviceId": serviceId,
      "locationId": 26
    });
    let responseJson = await new WebApi('PrivateTraining/serviceProviderLocation', jsonBody);
    // const response = responseJson.json();
    this.setState({data: responseJson.items});
  }

  render() {
    if (this.state.data === null) {
      <Loader/>;
    } else if (this.state.data.length === 0) {
      return (
        <View style={{ flex:1,justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: 20, fontFamily: 'IRANSansMobile_Bold'}}>خدمتیاری برای این خدمت وجود ندارد</Text>
        </View>
      );
    }

    return (
      <Container>
        <View style={styles.container}>
          <Modal animationType={'slide'} transparent={true}
                 visible={this.state.modalVisible}
                 onRequestClose={() => {
                   console.log('Modal has been closed.');
                 }}>
            <View style={styles.modal}>
              <Text style={styles.text}>کدام دسته از خدمتیاران را می بینید؟</Text>

              <RadioButton options={options}/>
              {/*/!*<PickerChoose currencies={currencies}/>*!/ todo repaire it*/}
              <Picker
                selectedValue={this.state.language}
                style={{height: 50, width: 100}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({language: itemValue})
                }>
                <Picker.Item label="سطح یک" value="1"/>
                <Picker.Item label="سطح دو" value="2"/>
                <Picker.Item label="سطح سه" value="3"/>
              </Picker>

              <Button onPress={() => {
                this.toggleModal(!this.state.modalVisible);
              }}
                      style={{borderRadius: 5}} rounded success>
                <Text style={{paddingLeft: 40, fontSize: 12, fontFamily: 'IRANSansMobile_Bold', paddingRight: 40}}>اعمال
                  فیلتر</Text>
              </Button>

            </View>
          </Modal>
        </View>

        <View style={styles.container}>
          <Modal animationType={'slide'} transparent={true}
                 visible={this.state.modalVisibleServant}
                 onRequestClose={() => {
                   console.log('Modal has been closed.');
                 }}>
            <View style={styles.modal}>

              <Button onPress={() => {
                this.toggleModal(!this.state.modalVisibleServant);
              }}
                      style={{borderRadius: 5}} rounded success>
                <Text style={{paddingLeft: 40, fontSize: 12, fontFamily: 'IRANSansMobile_Bold', paddingRight: 40}}>بستن</Text>
              </Button>

            </View>
          </Modal>
        </View>


        <Content style={{paddingTop: 10, backgroundColor: '#e2e0e1'}}>

          {this.renderServant()}

        </Content>
        <Footer>
          <FooterTab>
            <Button
              onPress={() => {this.toggleModal(true);}}
              success style={{flexDirection: 'row'}}>
              <Icon3 name="filter" size={30} style={{marginRight: 30, color: 'white'}}/>
              <Text style={{fontSize: 14, fontFamily: 'IRANSansMobile_Bold'}}>فیلتر کردن</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#ede3f2',
    padding: 0,
  },
  modal: {
    borderWidth: 3,
    flex: 0.6,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255,255,255,1)',
    marginTop: '30%',
    marginLeft: '10%',
    marginRight: '10%',
  },
  text: {
    color: '#3f2949',
    marginTop: 10,
    fontSize: 12,
    fontFamily: 'IRANSansMobile_Bold',
  },
});

