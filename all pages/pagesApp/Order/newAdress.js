import React from 'react';
import {Image, Text} from 'react-native';
import {Button, Picker, Item} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,
  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  ActionSheet,
  Right,
} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';
import Header from '../../../components/Header';
import OrderStatic from '../../../staticClass/OrderStatic';
import WebApi from '../../../conection/WebApi';

var BUTTONS = [
  {text: 'Option 0', icon: 'american-football', iconColor: '#2c8ef4'},
  {text: 'Option 1', icon: 'analytics', iconColor: '#f42ced'},
  {text: 'Option 2', icon: 'aperture', iconColor: '#ea943b'},
  {text: 'Delete', icon: 'trash', iconColor: '#fa213b'},
  {text: 'Cancel', icon: 'close', iconColor: '#25de5b'},
];
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;
export default class newAdress extends React.Component {
  static navigationOptions = ({navigation}) => ({
    headerTitle:
      <Header text={'آدرس جدید'}/>
    , headerLeft: null,
    headerStyle: {
      backgroundColor: '#785599',
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      address: '',
      phone: '',
    };
  }

  async addNewAddress() {
    let jsonBody = JSON.stringify({
      'addressJson': [
        {
          "id": "2",
          "phone": this.state.phone,
          'address': this.state.address,
          "cityId": "2"
        }]
    });


    let responseJson = await new WebApi('Account/UpdateUser', jsonBody);
    console.log(responseJson);
    const response = responseJson.json();
    // this.props.navigation.pop()
    // this.setState({data: responseJson.items});
  }

  render() {
    console.log();
    return (
      <Container style={{flex: 1, backgroundColor: 'rgb(226,224,225)'}}>
        <Content>
          <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 25}}>
            <Picker
              enabled={false}
              mode="dropdown"
              label="شهر"
              selectedValue={this.state.city}
              style={{height: 60, backgroundColor: 'white', borderRadius: 9, width: 300}}
              itemStyle={{fontFamily: 'IRANSansMobile'}}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({city: itemValue})
              }>
              {OrderStatic.listCity.map(item => {
                return <Picker.Item key={item.id} label={item.name} value={item.id}/>;
              })}
            </Picker>

            <Textarea
              onChangeText={address => this.setState({address})}
              style={{width: 300, backgroundColor: 'white', borderRadius: 9, fontFamily: 'IRANSansMobile'}}
              rowSpan={5} bordered
              placeholder="آدرس دقیق خود را بنویسید."/>

            <Textarea
              onChangeText={phone => this.setState({phone})}
              style={{width: 300, backgroundColor: 'white', borderRadius: 9, fontFamily: 'IRANSansMobile'}}
                      rowSpan={2} bordered
                      placeholder="تلفن همراه"/>
          </View>
        </Content>
        <Footer style={{marginTop: 140}}>
          <FooterTab>
            <Button
              onPress={() => this.addNewAddress()}
              style={{flex: 1, backgroundColor: 'rgb(61,166,73)', flexDirection: 'row'}} full>
              <Icon name="plus" style={{color: 'white'}} size={30}/>
              <Text
                style={{fontSize: 16, color: 'white', marginLeft: 10, fontFamily: 'IRANSansMobile_Bold'}}>افزودن</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>

    );
  }
}
