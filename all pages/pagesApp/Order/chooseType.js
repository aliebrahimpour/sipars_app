import React from 'react';
import {Image, ImageBackground, Text, View} from 'react-native';
import {Button, List, Right, Left} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Header from '../../../components/Header';
import OrderStatic from '../../../staticClass/OrderStatic'

export default class typeService extends React.Component {
  static navigationOptions = ({navigation}) => ({
    headerTitle:
      <Header text={'انتخاب خدمتیار '}/>
    , headerLeft: null,
    headerStyle: {
      backgroundColor: '#785599',
    },
  });


  render() {
    return (
      <View style={{backgroundColor: '#e2e0e1', flex: 1}}>
        <View style={{alignItems: 'center', marginTop: 30, marginBottom: 10}}>
          <Text style={{fontSize: 14, fontFamily: 'IRANSansMobile_Bold'}}>خدمات انتخابی شما</Text>
        </View>

        <View
          style={{
            paddingBottom: 10,
            marginLeft: 30,
            marginRight: 30,
            alignItems: 'center',
            backgroundColor: '#b289d5',
            borderRadius: 10,
          }}>
          <Text style={{fontSize: 25, fontFamily: 'IRANSansMobile_Bold'}}>{OrderStatic.service.title}</Text>
          <Text style={{fontSize: 20, fontFamily: 'IRANSansMobile_Bold', color: 'white'}}>{OrderStatic.serviceSelect.title}</Text>
          <Button
            onPress={() => this.props.navigation.navigate('viewTariff')}
            style={{
            marginTop: 10,
            width: 100,
            borderRadius: 25,
            backgroundColor: '#f0b209',
            justifyContent: 'center',
          }}>
            <Text style={{
              fontSize: 12,
              fontFamily: 'IRANSansMobile_Bold',
            }}>مشاهده تعرفه ها</Text>
          </Button>
        </View>

        <View style={{alignItems: 'center', marginTop: 50, marginBottom: 10}}>
          <Text style={{fontSize: 14, fontFamily: 'IRANSansMobile_Bold'}}>انتخاب خدمتیار</Text>
        </View>

        <Button
          onPress={() => this.props.navigation.navigate('chooseServant')}
          style={{
            borderWidth: 1, height: 65, flexDirection: 'column', backgroundColor: 'white',
            marginLeft: 30,
            marginBottom: 5,
            marginRight: 30,
            alignItems: 'center',
            borderRadius: 10,
          }} transparent>

          <Text style={{fontSize: 20, fontFamily: 'IRANSansMobile_Bold'}}>مشخصات خدمتیاران</Text>
          <Text style={{fontSize: 10, fontFamily: 'IRANSansMobile', marginTop: 5}}>انتخاب از خدمتیاران بر اساس مشخصات
            ، رزومه و تخفیفات</Text>
        </Button>

        <View
          style={{
            backgroundColor: 'white',
            borderWidth: 1,
            marginLeft: 30,
            marginBottom: 5,
            marginRight: 30,
            alignItems: 'center',
            borderRadius: 10,
          }}>
          <Button style={{height: 60, flexDirection: 'column'}} transparent>
            <Text style={{fontSize: 20, fontFamily: 'IRANSansMobile_Bold'}}>توسط سی پارس</Text>
            <Text style={{fontSize: 10, fontFamily: 'IRANSansMobile'}}>انتخاب شایسته ترین خدمتیار توسط سی پارس</Text>
          </Button>
        </View>

        <View
          style={{
            backgroundColor: 'white',
            borderWidth: 1,
            marginLeft: 30,
            marginBottom: 5,
            marginRight: 30,
            alignItems: 'center',
            borderRadius: 10,
          }}>
          <Button style={{height: 60, flexDirection: 'column'}} transparent>
            <Text style={{fontSize: 20, fontFamily: 'IRANSansMobile_Bold'}}>بهترین قیمت</Text>
            <Text style={{fontSize: 10, fontFamily: 'IRANSansMobile', marginTop: 5}}>انتخاب خدمتیار پس از دریافت قیمت
              های پیشنهادی</Text>
          </Button>
        </View>


      </View>
    );
  }
}

