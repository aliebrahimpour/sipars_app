import React from 'react';
import {Image, Picker, Text} from 'react-native';
import {Button, Item} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,
  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';
import Header from '../../../components/Header';
import WebApi from '../../../conection/WebApi';
import OrderStatic from '../../../staticClass/OrderStatic';


export default class chooseAddress extends React.Component {
  static navigationOptions = ({navigation}) => ({
    headerTitle:
      <Header text={'انتخاب آدرس'}/>
    , headerLeft: null,
    headerStyle: {
      backgroundColor: '#785599',
    },
  });

  constructor(props) {
    super(props);
    this.state = {address: []};
  }

  async componentDidMount(): Promise<void> {
    let responseJson = await new WebApi('Account/IsLogin');
    console.log(responseJson);
    // let response = responseJson.json();
    this.setState({address: responseJson.user.addressJson});
    this.forceUpdate();
  }


  getListAddress() {
    console.log('ali');
    console.log(this.state.address);
    if (this.state.address.length > 0) {
      let jsonAdress = JSON.parse(this.state.address);
      return jsonAdress.map((v) => {
        return <ListItem style={{height:100}}>
          <View>
            <Radio selected={false}/>
          </View>
          <View>
            <Text style={{fontSize: 12, marginLeft: 10, marginRight: 10, fontFamily: 'IRANSansMobile_Bold'}}>مشهد</Text>
            <Text style={{
              fontSize: 12,
              marginLeft: 10,
              marginRight: 10,
              fontFamily: 'IRANSansMobile_Bold',
            }}>{v.address}</Text>
            <Text style={{
              fontSize: 12,
              marginLeft: 10,
              marginRight: 10,
              fontFamily: 'IRANSansMobile_Bold',
            }}>{v.phone}</Text>
          </View>
        </ListItem>;
      });
    }
    return;
  }


  render() {

    return (
      <Container>
        <Content style={{backgroundColor: 'rgb(226,224,225)'}}>
          <Body>
          <Text style={{
            textAlign: 'left',
            fontSize: 16,
            marginBottom: 30,
            marginLeft: 10,
            marginTop: 10,
            fontFamily: 'IRANSansMobile_Bold',
          }}>لطفا یکی از آدرس ها را انتخاب نمایید:</Text>
          <Card>
            <List style={{width: 300}}>

              <Item style={{
                borderRadius: 28,
                backgroundColor: 'white',
                width: 100,
                borderColor: 'black',
                marginLeft: '35%',
              }}/>
              {this.getListAddress()}
            </List>
          </Card>

          <ListItem>
            <Button
              onPress={() => this.props.navigation.navigate('newAdress')}

              style={{backgroundColor: 'rgb(0,88,38)'}}>
              <Icon name="plus" style={{color: 'white'}} size={30}/>
              <Text style={{
                fontSize: 12,
                marginLeft: 10,
                marginRight: 10,
                color: 'white',
                fontFamily: 'IRANSansMobile_Bold',
              }}>افزودن
                آدرس جدید</Text>
            </Button>
          </ListItem>

          </Body>
        </Content>
        <Footer>
          <FooterTab>
            <Button
              onPress={() => this.props.navigation.navigate('orderSummery')}
              style={{flex: 1, backgroundColor: 'rgb(61,166,73)'}} full>
              <Text
                style={{fontSize: 16, color: 'white', marginLeft: 10, fontFamily: 'IRANSansMobile_Bold'}}>ادامه</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>

    );
  }
}
