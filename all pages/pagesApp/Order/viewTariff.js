import React from 'react';
import {FlatList, Image, Picker, Text} from 'react-native';
import {Button, Item} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,

  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
  Separator,
} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';
import Header from '../../../components/Header';
import WebApi from '../../../conection/WebApi';
import OrderStatic from '../../../staticClass/OrderStatic';
import Servant from './chooseServant';


export default class viewTariff extends React.Component {
  static navigationOptions = ({navigation}) => ({
    headerTitle:
      <Header text={'مشاهده تعرفه ها'}/>
    , headerLeft: null,
    headerStyle: {
      backgroundColor: '#785599',
    },
  });

  constructor(props) {
    super(props);
    this.state = {
      traiffList: '',
      listServiceWorkUnit:[],
    };
  }

  componentDidMount() {
    this.getListServiceLevel();
  }

  async getListServiceLevel() {
    let jsonBody = JSON.stringify({
      'Id': OrderStatic.serviceSelect.id,
    });
    try {
      const response = await fetch('http://api.sipars.ir/Account/ListServiceLevelPost/0', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonBody,
      });
      let responseList = await response.json();
      // console.log(responseList);
      let listServiceUnit = [];
      for (let item in responseList.Message) {
        let itemShow = responseList.Message[item];
        // console.log(itemShow);
        let allUnit = await this.getListServiceWorkUnit(itemShow.ServiceLevelListId);
        listServiceUnit = this.state.listServiceWorkUnit;
        listServiceUnit[item] = allUnit;
        this.setState({listServiceWorkUnit: listServiceUnit});
      }
      this.setState({traiffList: responseList});
    } catch (error) {
      console.log(error);
      alert(error);
    }
  }

  async getListServiceWorkUnit(ServiceLevelListId) {
    let jsonBody = JSON.stringify({
      'ServiceId': OrderStatic.serviceSelect.id,
      'LocationId': '26',
      'ServiceLevelListId': ServiceLevelListId,
    });
    try {
      const response = await fetch('http://api.sipars.ir/PrivateTrain/ServiceProperties/LoadServiceWorkUnit', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonBody,
      });
      let responseList = await response.json();
      // this.setState({listServiceWorkUnit: responseList});
      return responseList;
    } catch (error) {
      console.log(error);
      alert(error);
    }
  } //todo


  rendreItem(item) {
    let itemShow = item.item;
    let unit = this.state.listServiceWorkUnit[item.index];

    return (
      <View>
        <Separator bordered>
          <Text style={{fontFamily: 'IRANSansMobile_Bold'}}>{itemShow.ServiceLevelTitle}</Text>
        </Separator>
        {unit.WorkUnits.map(unitItem =>
          <ListItem>
            <Text style={{fontFamily: 'IRANSansMobile'}}>{unitItem.WorkUnitTitle} قیمت: {unitItem.PriceWorkUnit}</Text>
            <Text style={{fontFamily: 'IRANSansMobile'}}></Text>
          </ListItem>
        )}

      </View>
    );
  }

  renderList() {
    if (this.state.traiffList.length < 1) {
      return;
    }
    return (
      <FlatList
        data={this.state.traiffList.Message}
        renderItem={(item) =>
          this.rendreItem(item)
        }
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }

  render() {
    return (
      <Container style={{flex: 1, backgroundColor: 'rgb(226,224,225)'}}>
        <Content>

          {this.renderList()}


        </Content>
        <Footer>
          <FooterTab>
            <Button
              onPress={() => this.props.navigation.pop()}
              style={{flex: 1, backgroundColor: 'rgb(61,166,73)', flexDirection: 'row'}} full>
              <Icon name="plus" style={{color: 'white'}} size={30}/>
              <Text
                style={{fontSize: 16, color: 'white', marginLeft: 10, fontFamily: 'IRANSansMobile_Bold'}}>بازگشت</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>

    );
  }
}
