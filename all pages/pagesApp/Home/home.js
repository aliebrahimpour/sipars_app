import React from 'react';
import {
  Text,
  StatusBar,
  ScrollView,
  View,
  FlatList,
  Image,
  ListView,
  ImageBackground,
} from 'react-native';
import {Button, Content, List, Right, Left} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Header from '../../../components/Header';
import Loader from '../../../components/Loader';
import ChildItemBody from '../../../components/ChildItemBody';
import myService from '../MyServices/myService';
import WebApi from '../../../conection/WebApi';
import OrderStatic from '../../../staticClass/OrderStatic';

export default class Home extends React.Component {
  static navigationOptions = ({navigation}) => ({
    headerTitle: <Header text={'خدمات آنلاین سی پارس'} />,
    headerStyle: {
      backgroundColor: '#785599',
      height: 50,
    },
  });

  constructor(props) {
    super(props);

    this.state = {
      data: null,
    };
  }

  async componentDidMount(): Promise<void> {
    let responseJson = await new WebApi('privateTraining/MenuList');
    this.setState({data: responseJson.items});
  }

  renderChildItem(item) {
    return (
      <ChildItemBody
        data={this.state.data}
        navigation={this.props.navigation}
        item={item}
      />
    );
  }

  getRenderSpecailItem(title) {
    let childItem = WebApi.filterItems(this.state.data, 0, 1, true);

    return (
      <View>
        <View style={{flexDirection: 'row'}}>
          <Left>
            <View style={{marginLeft: 10, marginTop: 8}}>
              <Text style={{fontSize: 16, fontFamily: 'IRANSansMobile_Bold'}}>
                {title}
              </Text>
            </View>
          </Left>
          <Right>
            <Button
              style={{marginRight: 10}}
              onPress={() => {
                OrderStatic.service = item;
                this.props.navigation.navigate({
                  routeName: 'chooseService',
                  params: {
                    childItems: childItem,
                    data: this.state.data,
                    autoFocusSearch: true,
                  },
                });
              }}
              transparent>
              <Text
                style={{
                  textAlign: 'right',
                  fontSize: 14,
                  color: '#747474',
                  fontFamily: 'IRANSansMobile_Bold',
                }}>
                جستجو
              </Text>
            </Button>
          </Right>
        </View>

        <FlatList
          horizontal={true}
          style={{
            width: '100%',
            height: 100,
            paddingLeft: 0,
            paddingRight: 0,
            marginRight: 10,
          }}
          data={childItem}
          selected={false}
          renderItem={(item) => this.renderChildItem(item)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }

  // getRenderPublicItem(title) {
  //   let childItem = WebApi.filterItems(this.state.data, 0, 1, true);
  //
  //   return (
  //     <ScrollView>
  //       <View style={{flexDirection: 'row'}}>
  //         <View style={{marginLeft: 10}}>
  //           <Text style={{fontSize: 16, fontFamily: 'IRANSansMobile_Bold'}}>{title}</Text>
  //         </View>
  //         <View>
  //           <Text style={{
  //             textAlign: 'right',
  //             fontSize: 14,
  //             color: '#747474',
  //             marginLeft:180,
  //             fontFamily: 'IRANSansMobile_Bold',
  //           }}>جستجو</Text>
  //         </View>
  //       </View>
  //       <FlatList
  //         numColumns={3}
  //         // horizontal={true}
  //         style={{
  //           width: '100%',
  //           height: 400,
  //           paddingLeft: 0,
  //           paddingRight: 0,
  //         }}
  //         data={childItem}
  //         selected={false}
  //         renderItem={(item) =>
  //           this.renderChildItem(item)
  //         }
  //         keyExtractor={(item, index) => index.toString()}
  //       />
  //
  //
  //     </ScrollView>
  //   );
  // }

  render() {
    let data = null;
    data = this.state.data;

    if (data === null) {
      return <Loader />;
    } else {
      return (
        <View style={{flex: 1}}>
          <StatusBar backgroundColor="#d2b5eb" barStyle="light-content" />

          <View
            style={{
              alignItems: 'center',
              marginTop: 10,
              marginLeft: 10,
              borderWidth: 1,
              borderRadius: 10,
              marginRight: 10,
            }}>
            <Image
              source={{uri: 'http://api.sipars.ir/assets/New/img/slide/3.png'}}
              style={{height: 110, borderRadius: 10, width: '100%'}}
            />
            {/*<Image source={require('../../../assets/images/bg.png')} style={{height: 45, width: 70}}/>*/}
          </View>

          <View
            style={{
              alignItems: 'center',
              marginTop: 5,
              marginLeft: 10,
              borderWidth: 1,
              borderRadius: 10,
              marginRight: 10,
            }}>
            <Image
              source={{
                uri: 'http://api.sipars.ir/assets/New/img/service/3.jpg',
              }}
              style={{height: 80, borderRadius: 10, width: '100%'}}
            />
            {/*<Image source={require('../../../assets/images/bg.png')} style={{height: 45, width: 70}}/>*/}
          </View>

          <View
            style={{
              borderWidth: 2,
              marginLeft: 30,
              marginTop: 5,
              width: '78%',
              borderColor: '#8d8d8d',
            }}
          />

          {this.getRenderSpecailItem('خدمات ویژه')}
          {this.getRenderSpecailItem('دسته بندی خدمات')}
        </View>
      );
    }
  }
}
