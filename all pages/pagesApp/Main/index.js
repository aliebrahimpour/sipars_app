import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {I18nManager} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/SimpleLineIcons';
import Icon3 from 'react-native-vector-icons/Ionicons';
import Icon4 from 'react-native-vector-icons/MaterialCommunityIcons';
// import {Container, Header, Content, Footer, Text, View} from 'native-base';
import Account from '../Account/index';
import Club from '../Club/index';
import Home from '../Home/index';
import MyServices from '../MyServices/index';
import Order from '../Order/index';
import styles from '../../../assets/style/global';

I18nManager.forceRTL(true);

EStyleSheet.build({
  $colorDark: '#333',
});


let allPages = {
  Home: {screen: Home},
  Account: {screen: Account},
  Club: {screen: Club},
  MyServices: {screen: MyServices},
  Order: {screen: Order},
};

function myCreateStackNavigator(initialRouteName, navigationOptions) {
  return createStackNavigator(allPages,
    {
      initialRouteName: initialRouteName,
      headerMode: 'none',
      navigationOptions: {
        headerVisible: false,
      },
    });
}


const TabNavigator = createBottomTabNavigator({
  Home: {
    screen: myCreateStackNavigator('Home'),
    navigationOptions: {
      tabBarLabel: 'خانه',
      tabBarIcon: ({tintColor}) => <Icon name="home" size={30} color={tintColor}
                                         style={[styles.tabIcon, {color: tintColor}]}/>,
    },
  },
  MyServices: {
    screen: myCreateStackNavigator('MyServices'),
    navigationOptions: {
      tabBarLabel: 'خدمات من',
      tabBarIcon: ({tintColor}) => <Icon3 name="ios-list" size={30} color={tintColor}
                                          style={[styles.tabIcon, {color: tintColor}]}/>,
    },
  },

  Order: {
    screen: myCreateStackNavigator('Order'),
    navigationOptions: {
      tabBarLabel: 'سفارش خدمت',
      tabBarIcon: ({tintColor}) => <Icon3 name="md-add-circle-outline" size={30} color={tintColor}
                                          style={[styles.tabIcon, {color: tintColor}]}/>,
    },
  },


  Club: {
    screen: myCreateStackNavigator('Club'),
    navigationOptions: {
      tabBarLabel: 'باشگاه مشتریان',
      tabBarIcon: ({tintColor}) => <Icon4 name="account-group-outline" size={30} color={tintColor}
                                          style={[styles.tabIcon, {color: tintColor}]}/>,
    },
  },
  Account: {
    screen: myCreateStackNavigator('Account'),
    navigationOptions: {
      tabBarLabel: 'حساب کاربری',
      tabBarIcon: ({tintColor}) => <Icon2 name="user" size={30} color={tintColor}
                                          style={[styles.tabIcon, {color: tintColor}]}/>,
    },
  },

}, {
  tabBarOptions: {
    activeTintColor: 'rgba(0,0,0,1)',
    inactiveTintColor: 'rgba(0,0,0,.3)',
    inactiveBackgroundColor: '#d2b5eb',
    style: {
      backgroundColor: '#916bb6',
      fontSize: 20,
      fontFamily: 'IRANSansMobile'
    },
  },
});

export default createAppContainer(TabNavigator);



