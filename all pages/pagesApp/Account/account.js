import React from 'react';
import {Image, ImageBackground, Text, View} from 'react-native';
import {Button, List, Right, Left, Item, Input} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icon3 from 'react-native-vector-icons/Ionicons';
import Header from '../../../components/Header';

export default class account extends React.Component {
  static navigationOptions = ({navigation}) => ({
    headerTitle:
      <Header text={'پروفایل کاربری'}/>
    ,
    headerStyle: {
      backgroundColor: '#785599',
    },
  });

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#e0e1e1'}}>

        <View style={styles._profileImage.container}>
          <Image source={require('../../../assets/images/bg.png')} style={styles._profileImage.image}/>
          <Icon2 name="camera" color="#3498db" style={styles._profileImage.icon} size={30}/>
        </View>

        <Item style={{marginTop: 70, marginLeft: 90, alignItems: 'center', justifyContent: 'center'}}>
          <Icon2 size={30} name='account'/>
          <Input style={{fontSize: 14, marginLeft: 5, fontFamily: 'IRANSansMobile_Bold'}}
                 placeholder='نام و نام خانوادگی'/>
          <Button transparent style={{marginRight:100}}>
            <Text style={{fontSize: 12, fontFamily: 'IRANSansMobile_Bold'}}
            >ویرایش</Text>
          </Button>

        </Item>
        <View style={{borderWidth: 1,marginLeft:90, width: '60%', borderColor: 'gray'}}/>

        <Item style={{marginLeft: 125, alignItems: 'center', justifyContent: 'center'}}>
          <Icon2 size={30} name='phone'/>
          <Input style={{fontSize: 14, marginLeft: 25, fontFamily: 'IRANSansMobile_Bold'}}
                 placeholder='شماره تلفن'/>
        </Item>

        <View style={{borderWidth: 1,marginLeft:90, width: '60%', borderColor: 'gray'}}/>


        <Item style={{alignItems: 'stretch',marginTop:30,marginLeft:90, justifyContent: 'flex-start'}}>
          <Icon size={30} color="#785599" name='credit-card'/>
          <Text style={{fontSize: 10, marginLeft: 5, fontFamily: 'IRANSansMobile_Bold'}}
          >اعتبار کیف پول</Text>
          <Text style={{fontSize: 10, marginLeft: 5, fontFamily: 'IRANSansMobile_Bold'}}
          >20/000 هزار تومان</Text>
          <Button success style={{marginLeft:30,height:30,width:40,justifyContent:"center"}}>
            <Text style={{fontSize: 10,textAlign:"center", fontFamily: 'IRANSansMobile_Bold'}}
            >افزایش</Text>
          </Button>
        </Item>

        <View style={{borderWidth: 1,marginLeft:90, width: '60%', borderColor: 'gray'}}/>

        <Item style={{alignItems: 'stretch',marginTop:10,marginLeft:90, justifyContent: 'flex-start'}}>
          <Icon size={30} color="#785599" name='share-google'/>
          <Text style={{fontSize: 10, marginLeft: 5, fontFamily: 'IRANSansMobile_Bold'}}
          >معرفی به دوستان</Text>
        </Item>

        <View style={{borderWidth: 1,marginLeft:90, width: '60%', borderColor: 'gray'}}/>

        <Item style={{alignItems: 'stretch',marginTop:10,marginLeft:90, justifyContent: 'flex-start'}}>
          <Icon2 size={30} color="#785599" name='file-document-box-outline'/>
          <Text style={{fontSize: 10, marginLeft: 5, fontFamily: 'IRANSansMobile_Bold'}}
          >قوانین و مقررات</Text>
        </Item>

        <View style={{borderWidth: 1,marginLeft:90, width: '60%', borderColor: 'gray'}}/>

        <Item style={{alignItems: 'stretch',marginTop:10,marginLeft:90, justifyContent: 'flex-start'}}>
          <Icon2 size={30} color="#785599" name='contacts'/>
          <Text style={{fontSize: 10, marginLeft: 5, fontFamily: 'IRANSansMobile_Bold'}}
          >تماس با ما</Text>
        </Item>

        <View style={{borderWidth: 1,marginLeft:90, width: '60%', borderColor: 'gray'}}/>

        <Item style={{alignItems: 'stretch',marginTop:10,marginLeft:90, justifyContent: 'flex-start'}}>
          <Icon2 size={30} color="#785599" name='information-outline'/>
          <Text style={{fontSize: 10, marginLeft: 5, fontFamily: 'IRANSansMobile_Bold'}}
          >به روزرسانی</Text>
          <Text style={{marginLeft:120,fontSize: 10,textAlign:"center", fontFamily: 'IRANSansMobile_Bold'}}
          >نسخه 1</Text>
        </Item>

        <View style={{borderWidth: 1,marginLeft:90, width: '60%', borderColor: 'gray'}}/>



        <Button style={{marginLeft:70,marginRight:70,paddingLeft: 30, paddingRight: 30,justifyContent:"center",marginTop:30}} danger>
          <Text style={{fontSize: 12, marginLeft: 5, fontFamily: 'IRANSansMobile_Bold'}}
          >خروج از حساب کاربری</Text>
        </Button>
      </View>
    );
  }
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  _profileInfo: {
    container: {
      flex: .7,
    },
    info: {
      flexDirection: 'row',
      flex: 4,
      justifyContent: 'space-between',
      margin: 15,
    },
  },
  infoTextNumber: {
    '@media ios': {
      fontFamily: 'IRANSansMobile',
      fontWeight: 'bold',
    },
    '@media android': {
      fontFamily: 'IRANSansMobile_Bold',
    },
    fontSize: 16,
    color: '#222',
    textAlign: 'center',
  },
  infoText: {
    '@media ios': {
      fontFamily: 'IRANSansMobile',
      fontWeight: '300',
    },
    '@media android': {
      fontFamily: 'IRANSansMobile_Light',
    },
    textAlign: 'center',
  },
  _profileImage: {
    container: {
      flex: .3,
      alignItems: 'center',
      marginTop: 15,

    },
    image: {
      width: 80,
      height: 80,
      borderRadius: 40,
      borderColor: '#888',
      borderWidth: 2,
      margin: 5,
    },
    icon: {
      backgroundColor: '#785599',
      bottom: 35,
      left: 30,
      borderRadius: 18,
      overflow: 'hidden',
      color: 'white',
    },
  },
  profileName: {
    '@media ios': {
      fontFamily: 'IRANSansMobile',
      fontWeight: 'bold',
    },
    '@media android': {
      fontFamily: 'IRANSansMobile_Bold',
    },
    fontSize: 16,
    marginLeft: 10,
    color: '#333',
  },
  headerRightStyle: {
    flexDirection: 'row',
    width: 85,
    marginRight: 10,
    justifyContent: 'space-between',
  },
});
