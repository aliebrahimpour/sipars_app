import React from 'react';
import {Image, Picker, ScrollView, Text} from 'react-native';
import {Button, Item, Thumbnail} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,
  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
  Tab, Tabs, TabHeading,
  ScrollableTab,

} from 'native-base';
import {Col, Grid, Row} from 'react-native-easy-grid';
import Header from '../../../components/Header';

export default class club extends React.Component {
  static navigationOptions = ({navigation}) => ({
    headerTitle:
      <Header text={'باشگاه مشتریان'}/>
    ,
    headerStyle: {
      backgroundColor: '#785599',
    },
  });


  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#e0e1e1', alignItems: 'center'}}>

        <View style={{marginTop: 70, flexDirection: 'row', height: 70}}>
          <Image source={require('../../../assets/images/bg.png')} style={{height: 50, width: 50}}/>
          <Text style={{fontSize: 18, marginLeft: 10, fontFamily: 'IRANSansMobile_Bold'}}>علی ابراهیم پور</Text>
        </View>

        <View style={{flexDirection: 'row'}}>
          <Icon2 name="podium-gold" style={{marginRight: 15, color: 'gold'}} size={30}/>
          <Text style={{fontSize: 14, fontFamily: 'IRANSansMobile_Bold'}}>کاربر طلایی</Text>
        </View>

        <View style={{marginTop: 10, backgroundColor: '#8b4bc6', flexDirection: 'row'}}>
          <Icon2 name="brightness-percent" style={{
            marginLeft: 15,
            color: 'white',
            alignItems: 'center',
            textAlign: 'center',
            justifyContent: 'center',
          }} size={28}/>
          <Text style={{
            fontSize: 18,
            paddingLeft: 10,
            paddingRight: 10,
            color: 'white',
            fontFamily: 'IRANSansMobile_Bold',
          }}>تخفیف شما: فلان درصد</Text>
        </View>
        <View style={{marginTop: 30}}>
          <Text style={{fontSize: 14, fontFamily: 'IRANSansMobile_Bold'}}>برترین ها</Text>
        </View>

        <View style={{borderWidth: 2, width: '78%', borderColor: '#8b4bc6'}}/>

        <Grid style={{justifyContent: 'center', alignItems: 'center', marginLeft: 60}}>

          <Col>
            <Row>
              <Text style={{
                textAlign: 'center',
                fontSize: 14,
                color: 'gray',
                fontFamily: 'IRANSansMobile_Bold',
              }}>رتبه</Text>
            </Row>
            <Row>
              <Text
                style={{textAlign: 'center', fontSize: 14, color: 'gray', fontFamily: 'IRANSansMobile_Bold'}}>1</Text>
            </Row>
          </Col>

          <Col>
            <Row>
              <Text style={{fontSize: 14, textAlign: 'center', color: 'gray', fontFamily: 'IRANSansMobile_Bold'}}>شماره
                تلفن </Text>
            </Row>
            <Row>
              <Text style={{
                fontSize: 14,
                textAlign: 'center',
                color: 'gray',
                fontFamily: 'IRANSansMobile_Bold',
              }}>0000000 </Text>
            </Row>

          </Col>

          <Col>
            <Row>
              <Text style={{
                fontSize: 14,
                textAlign: 'center',
                color: 'gray',
                fontFamily: 'IRANSansMobile_Bold',
              }}>امتیاز</Text>
            </Row>
            <Row>
              <Text style={{
                fontSize: 14,
                textAlign: 'center',
                color: 'gray',
                fontFamily: 'IRANSansMobile_Bold',
              }}>00000 </Text>
            </Row>
          </Col>

        </Grid>


        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 14, fontFamily: 'IRANSansMobile_Bold'}}>رتبه شما</Text>
        </View>

        <View style={{borderWidth: 2, width: '78%', borderColor: '#8b4bc6'}}/>
        <Grid style={{justifyContent: 'center', alignItems: 'center', marginLeft: 60}}>

          <Col>
            <Row>
              <Text style={{
                textAlign: 'center',
                fontSize: 14,
                color: 'gray',
                fontFamily: 'IRANSansMobile_Bold',
              }}>رتبه</Text>
            </Row>
            <Row>
              <Text
                style={{textAlign: 'center', fontSize: 14, color: 'gray', fontFamily: 'IRANSansMobile_Bold'}}>1</Text>
            </Row>
          </Col>

          <Col>
            <Row>
              <Text style={{fontSize: 14, textAlign: 'center', color: 'gray', fontFamily: 'IRANSansMobile_Bold'}}>شماره
                تلفن </Text>
            </Row>
            <Row>
              <Text style={{
                fontSize: 14,
                textAlign: 'center',
                color: 'gray',
                fontFamily: 'IRANSansMobile_Bold',
              }}>شما </Text>
            </Row>

          </Col>

          <Col>
            <Row>
              <Text style={{
                fontSize: 14,
                textAlign: 'center',
                color: 'gray',
                fontFamily: 'IRANSansMobile_Bold',
              }}>امتیاز</Text>
            </Row>
            <Row>
              <Text style={{
                fontSize: 14,
                textAlign: 'center',
                color: 'gray',
                fontFamily: 'IRANSansMobile_Bold',
              }}>00000 </Text>
            </Row>
          </Col>

        </Grid>

        <Button style={{
          backgroundColor: '#8b4bc6',
          marginLeft: 70,
          marginRight: 70,
          paddingLeft: 30,
          paddingRight: 30,
          justifyContent: 'center',
          marginTop: 30,
          marginBottom: 40,
        }}>
          <Text style={{fontSize: 14, marginLeft: 5, fontFamily: 'IRANSansMobile_Bold', color: 'white'}}
          >راهنمای امتیازگیری</Text>
        </Button>


      </View>
    );
  }
}

