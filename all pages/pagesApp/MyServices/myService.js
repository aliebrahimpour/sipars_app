import React from 'react';
import {Image, Picker, ScrollView, Text} from 'react-native';
import {Button, Item, Thumbnail} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,
  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
  Tab, Tabs, TabHeading,

} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';
import Tab1 from '../../../components/exampleItam';
import Tab2 from '../../../components/exampleItam';
import Tab3 from '../../../components/exampleItam';
import Header from '../../../components/Header';
import WebApi from '../../../conection/WebApi';

export default class MyService extends React.Component {
  initialize(){
    this.state={
      pageNumber: 1
    }
  }


  async componentDidMount(): Promise<void> {
    let jsonBody = JSON.stringify({
      "serviceId": serviceId,
      "locationId": 26
    });
    let responseJson = await new WebApi('PrivateTraining/serviceProviderLocation', jsonBody);
  }


  static navigationOptions = ({navigation}) => ({
    headerTitle:
      <Header text={'خدمات من'}/>
    ,
    headerStyle: {
      backgroundColor: '#785599',
    },
  });




  render() {
    return (
      <Container>

        <View style={{flex: 1, alignItems: 'center', backgroundColor: 'rgb(226,224,225)'}}>

          <View style={{marginTop: 60, width: '90%'}}>
            <ScrollView >
              <Tabs>
                <Tab heading={<TabHeading><Text
                  style={{fontSize: 16, color: 'white', fontFamily: 'IRANSansMobile_Bold'}}>جدید</Text></TabHeading>}>
                  <Tab1/>
                </Tab>

                <Tab
                  heading={<TabHeading><Text style={{fontSize: 16, color: 'white', fontFamily: 'IRANSansMobile_Bold'}}>در
                    دست اقدام</Text></TabHeading>}>
                  <Tab2/>
                </Tab>

                <Tab
                  heading={<TabHeading><Text style={{fontSize: 16, color: 'white', fontFamily: 'IRANSansMobile_Bold'}}>تمام
                    شده</Text></TabHeading>}>
                  <Tab3/>
                </Tab>
              </Tabs>
            </ScrollView>
          </View>

        </View>
      </Container>
    );
  }
}

