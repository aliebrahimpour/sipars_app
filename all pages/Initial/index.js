import React, {Component} from 'react';
import Login from './Login';
import Register from './Register';
import Verify from './Verify';
import ForgetPassword from './ForgetPassword';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import NavigationMenu from '../../all pages/pagesApp/Main/index';


let App = createStackNavigator({
    Register: {screen: Register},
    Verify: {screen: Verify},
    Login: {screen: Login},
    ForgetPassword : {screen: ForgetPassword},
    Home: {screen: NavigationMenu},
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    },
  });

const AppContainer = createAppContainer(App);
export default AppContainer;

