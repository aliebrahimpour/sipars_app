import React from 'react';
import {TextInput, View, StyleSheet, ImageBackground} from 'react-native';
import global from '../../assets/style/global';
import EStyleSheet from 'react-native-extended-stylesheet';
import {Container, Header, Badge, Text, Content, Button, Item, Input} from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/SimpleLineIcons';
import Main from '../pagesApp/Main/index';
import AsyncStorage from '@react-native-community/async-storage';
import {StackActions, NavigationActions} from 'react-navigation';
import WebApi from '../../conection/WebApi';
import App from '../../app';

export default class Verify extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      timer: 59,
      activationCode: '',
      username : this.props.navigation.getParam('username'),
    };
  }

  componentDidMount() {
    this.interval = setInterval(
      () => this.setState((prevState) => ({timer: prevState.timer - 1})),
      1000,
    );
  }

  componentDidUpdate() {
    if (this.state.timer === 1) {
      clearInterval(this.interval);
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  showTimer(timer) {
    if (timer !== 1) {
      return <View style={{flexDirection: 'row', paddingTop: 5, justifyContent: 'center', alignItems: 'center'}}>
        <Badge style={{backgroundColor: 'white', alignItems: 'center'}}>
          <Text style={{color: 'black'}}> 0</Text>
        </Badge>
        <Text style={{color: 'black'}}> : </Text>

        <Badge style={{backgroundColor: 'white', alignItems: 'center'}}>
          <Text style={{color: 'black'}}> {this.state.timer} </Text>
        </Badge>
      </View>;
    } else {
      return <Button style={{
        width: 100,
        borderRadius: 28,
        backgroundColor: '#2e1954',
        justifyContent: 'center',
      }}>
        <Text style={{
          fontSize: 10,
          fontFamily: 'IRANSansMobile',
          color: 'white',
        }}>ارسال دوباره</Text>
      </Button>;
    }
  }


   changeToken() {
    AsyncStorage.setItem('isLoginChecked', JSON.stringify(true));
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'Home'})],
    });
    this.props.navigation.dispatch(resetAction);
  }

  async checkVerify(activationCode) {
    let jsonBody = JSON.stringify({
      'username': this.state.username,
      'activationCode': activationCode
    });
    let responseJson = await new WebApi('Account/ActiveCodeCustomer', jsonBody);
    if (responseJson.result === 'done') {
      return this.changeToken();
    }
  }

  render() {
    return (
      <ImageBackground source={require('../../assets/images/login.jpg')} style={{width: '100%', height: '100%'}}
                       imageStyle={{opacity: 0.1}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>

        <Item>
          <Text style={{
            fontSize: 25,
            fontFamily: 'IRANSansMobile_Bold',
            marginBottom: 100,
          }}>کد فعالسازی</Text>
        </Item>

        <Item style={{
          borderRadius: 28,
          backgroundColor: 'white',
          width: 260,
          borderColor: 'black',
          marginBottom: 10,
        }}>
          <Icon style={{paddingRight: 10}} size={25} name='key'/>
          <Input
            onChangeText={(activationCode) => this.setState({activationCode})}
            keyboardType="numeric"
            style={{
              fontFamily: 'IRANSansMobile',
              textAlign: 'center',
              paddingLeft: 20,
            }}
            placeholder={'کد فعالسازی'}/>
        </Item>


        <Button
          onPress={() => this.checkVerify(this.state.activationCode)}
          style={{
            width: 260,
            borderRadius: 28,
            backgroundColor: '#2e1954',
            justifyContent: 'center',
          }}>
          <Text style={{
            fontSize: 15,
            fontFamily: 'IRANSansMobile',
            color: 'white',
            paddingRight: 30,
          }}>تایید</Text>
        </Button>

        <View style={{paddingTop: 30, justifyContent: 'center', alignItems: 'center'}}>
          <Text style={[styles.splash_text, {color: 'black'}]}>دریافت مجدد کد فعال سازی</Text>
        </View>

        {this.showTimer(this.state.timer)}

        <View style={{marginTop: 60, backgroundColor: 'rgba(46,25,84,0.6)', borderRadius: 5, width: 200}}>
          <Button onPress={() => this.props.navigation.pop()}
                  style={{height: 30, justifyContent: 'center', alignItems: 'center'}} transparent>
            <Text style={styles.splash_text}>شماره را اشتباه وارد کردید؟</Text>
          </Button>
        </View>

      </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  splash_text: {
    fontFamily: 'IRANSansMobile',
    color: 'white',
    textAlign: 'center',
    fontSize: 12,
  },

});
