import React from 'react';
import {TextInput, View, StyleSheet, ImageBackground} from 'react-native';
import global from '../../assets/style/global';
import EStyleSheet from 'react-native-extended-stylesheet';
import {Container, Header, Text, Content, Button, Item, Input} from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/SimpleLineIcons';
import WebApi from '../../conection/WebApi';

export default class ForgetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkRegister: false,
      username: '',
    };
  }


  async AccountForgetPassword(username) {
    try {
      let response = await fetch('http://api.sipars.ir/Account/ForgotPasswords', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          'UserName': username,
          'Mobile': username,
        }),
      });
      let responseJson = await response.json();
      if (responseJson.Resualt) {
        this.setState({
          person: responseJson,
          checkRegister: true,
        });
      }
      if (this.state.checkRegister) {
        return this.props.navigation.navigate({routeName: 'Login', params: {username: username}});
      } else {
        return this.state.checkRegister;
      }
    } catch (error) {
      alert(error);
      console.error(error);
    }
  }


  render() {
    return (
      <ImageBackground source={require('../../assets/images/login.jpg')} style={{width: '100%', height: '100%'}}
                       imageStyle={{opacity: 0.1}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>

          <Item>
            <Text style={{
              fontSize: 25,
              fontFamily: 'IRANSansMobile_Bold',
              paddingBottom: 30,
            }}>فراموشی رمز عبور</Text>
          </Item>


          <Item style={{
            borderRadius: 28,
            backgroundColor: 'white',
            width: 260,
            borderColor: 'black',
            marginBottom: 10,
          }}>
            <Icon2 style={{paddingRight: 10}} size={25} name='phone'/>
            <Input
              keyboardType="numeric"
              onChangeText={(username) => this.setState({username})}
              style={{
                fontFamily: 'IRANSansMobile',
                textAlign: 'center',
                paddingLeft: 20,
              }}
              placeholder={'تلفن همراه'}
            />
          </Item>


          <Button
            onPress={() => this.AccountForgetPassword(this.state.username)}
            style={{
              width: 260,
              borderRadius: 28,
              backgroundColor: '#2e1954',
              justifyContent: 'center',
            }}>
            <Text style={{
              fontSize: 15,
              fontFamily: 'IRANSansMobile',
              color: 'white',
              paddingRight: 30,
            }}>دریافت کد فعالسازی</Text>
          </Button>

        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  splash_text: {
    fontFamily: 'IRANSansMobile',
    color: 'white',
    textAlign: 'center',
    fontSize: 12,
  },

});
