import React, {Component} from 'react';
import {TextInput, View, StyleSheet, ImageBackground, Image, ToastAndroid} from 'react-native';
import global from '../../assets/style/global';
import EStyleSheet from 'react-native-extended-stylesheet';
import {Container, Toast, Root, Header, Text, Content, Button, Item, Input} from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/SimpleLineIcons';
import allPages from './index';
import WebApi from '../../conection/WebApi';
import AsyncStorage from '@react-native-community/async-storage';
import {NavigationActions, StackActions} from 'react-navigation';
import App from '../../app';


export default class Login extends Component {

  constructor(props) {
    super(props);
    AsyncStorage.setItem('isLoginChecked', JSON.stringify(false));
    this.state = {
      statusLogin: false, //todo for desgin
      allPages: allPages,
      checkLogin: false,
      person: [],
      username: 'mobileNumber',
      password: 'password',
    };
  }

  static showToast(message) {
    return (
      ToastAndroid.show(message, ToastAndroid.SHORT)
    );
  }

  async checkAccountLogin(username, password) {
    let jsonBody = JSON.stringify({
      'username': username,
      'password': password,
    });
    let responseJson = await new WebApi('Account/Login', jsonBody);
    console.log(responseJson);
    if (responseJson.result === 'done') {
      this.setState({
        person: responseJson,
        checkLogin: true,
      });
    }
    if (this.state.checkLogin) {
      return this.changeToken();
    } else {
      return Login.showToast(responseJson.message);
    }
  }

  changeToken() {
    AsyncStorage.setItem('isLoginChecked', JSON.stringify(true));
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({routeName: 'Home'})],
    });
    this.props.navigation.dispatch(resetAction);
  }

  _checkDisplay(field) {
    return {display: field !== '' ? 'none' : 'flex'};
  }

  render() {
    return (


      <ImageBackground source={require('../../assets/images/login.jpg')} style={{width: '100%', height: '100%'}}
                       imageStyle={{opacity: 0.1}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>

          <Item error style={{
            borderRadius: 28,
            backgroundColor: 'white',
            width: 260,
            borderColor: 'black',
            marginBottom: 10,
          }}>
            <Icon style={{paddingRight: 10}} size={25} name='user'/>
            <Input
              onChangeText={(username) => this.setState({username})}
              keyboardType="numeric"
              style={{
                fontFamily: 'IRANSansMobile',
                textAlign: 'center',
                paddingRight: 20,
              }}
              placeholder={'شماره موبایل'}
            />
          </Item>
          <Text style={[{
            fontSize: 11,
            color: 'red',
            fontFamily: 'IRANSansMobile_Bold',
          }, this._checkDisplay(this.state.username)]}>پر کردن این فیلد الزامی است</Text>


          <Item style={{
            borderRadius: 28,
            backgroundColor: 'white',
            width: 260,
            borderColor: 'black',
            marginBottom: 10,
          }}>
            <Icon2 style={{paddingRight: 10}} size={25} name='lock'/>
            <Input
              onChangeText={(password) => this.setState({password})}
              style={{
                fontFamily: 'IRANSansMobile',
                textAlign: 'center',
                paddingRight: 20,
              }}
              secureTextEntry
              placeholder={'رمز عبور'}
            />
          </Item>
          <Text style={[{
            fontSize: 11,
            color: 'red',
            fontFamily: 'IRANSansMobile_Bold',
          }, this._checkDisplay(this.state.password)]}>پر کردن این فیلد الزامی است</Text>


          <Button
            onPress={() => {
              this.checkAccountLogin(this.state.username, this.state.password);
            }}
            style={{
              width: 260,
              borderRadius: 28,
              backgroundColor: '#2e1954',
              justifyContent: 'center',
            }}>
            <Text style={{
              fontSize: 20,
              fontFamily: 'IRANSansMobile',
              color: 'white',
              paddingRight: 30,
            }}>ورود</Text>
          </Button>

          <View style={{
            marginTop: 20,
            alignItems: 'center',
            backgroundColor: 'rgba(46,25,84,0.6)',
            borderRadius: 5,
            width: 200,
          }}>
            <Button transparent
                    onPress={() => this.props.navigation.navigate('Register')}
            >
              <Text style={styles.splash_text}>حساب کاربری ندارید؟ ثبت نام</Text>
            </Button>
          </View>

          <View style={{
            marginTop: 20,
            alignItems: 'center',
            backgroundColor: 'rgba(46,25,84,0.6)',
            borderRadius: 5,
            width: 150,
          }}>
            <Button transparent
                    onPress={() => this.props.navigation.navigate('ForgetPassword')}
            >
              <Text style={styles.splash_text}>فراموشی رمز عبور</Text>
            </Button>
          </View>

        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  splash_text: {
    fontFamily: 'IRANSansMobile',
    color: 'white',
    fontSize: 12,
  },

});
