import React from 'react';
import {Text, View,StyleSheet} from "react-native";
import {Spinner} from 'native-base'
import global from '../../assets/style/global'
import EStyleSheet from "react-native-extended-stylesheet";

export default class Splash extends React.Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#916bb6', justifyContent: 'center', alignItems: 'center'}}>
        <View
          style={{
            marginTop:150,
            borderColor: '#6a418f',
            borderWidth: 3,
            width:"40%"
          }}
        />
        <Text style={styles.splash_text}>خدمات</Text>
        <Text style={styles.splash_text}>آنلاین</Text>
        <Text style={styles.splash_text}>سی پارس</Text>
        <View
          style={{
            marginTop:15,
            borderColor: '#6a418f',
            borderWidth: 3,
            width:"40%"
          }}
        />
        <Spinner />

        <Text style={styles.splash_link}>s i p a r s . i r</Text>
        <Text style={styles.splash_version}>نسخه 1/0</Text>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  splash_text: {
    fontSize: 40,
    color: "white",
    fontFamily: 'IRANSansMobile_Bold'
  },
  splash_link: {
    fontSize: 20,
    color: "white",
    fontFamily: 'IRANSansMobile'
  },
  splash_version: {
    marginTop:150,
    fontSize: 15,
    color: "white",
    fontFamily: 'IRANSansMobile'
  },

});

