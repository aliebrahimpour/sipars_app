import React, {Component} from 'react';
import {Text, View, TouchableOpacity, StyleSheet, Image, Modal} from 'react-native';
import {Button, Picker, Icon, Left, Right, Container} from 'native-base';
import Icon2 from 'react-native-vector-icons/EvilIcons';
import RadioButton from '../components/RadioButtons';
import WebApi from '../conection/WebApi';
import OrderStatic from '../staticClass/OrderStatic';

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      listCity: '',
    };
  }

  toggleModal(visible) {
    this.setState({modalVisible: visible});
  }

  async componentDidMount(): Promise<void> {
    let responseJson = await new WebApi('base/cityList');
    OrderStatic.listCity = responseJson.items;
    this.setState({listCity: responseJson.items});
  }


  render() {
    return (
      <View style={{flexDirection: 'row', marginLeft: 10}}>
        <View style={styles.container}>
          <Modal animationType={'slide'} transparent={true}
                 visible={this.state.modalVisible}
                 onRequestClose={() => {
                   console.log('Modal has been closed.');
                 }}>
            <View style={styles.modal}>
              <Text style={styles.text}>لطفا شهر مورد نظر خود را انتخاب کنید:</Text>

              <RadioButton style={{height: 50, width: 10, borderWidth: 5}} options={this.state.listCity}/>
              <Button onPress={() => {
                this.toggleModal(!this.state.modalVisible);
              }}
                      style={{borderRadius: 5}} rounded success>
                <Text style={{
                  paddingLeft: 40,
                  fontSize: 14,
                  fontFamily: 'IRANSansMobile_Bold',
                  paddingRight: 40,
                }}>اعمال</Text>
              </Button>

            </View>
          </Modal>
        </View>
        <Left style={{flexDirection: 'row'}}>
          <View style={{height: 50, width: 80}}>
            <Image source={require('../assets/images/logo.png')} style={{height: 45, width: 70}}/>
          </View>
        </Left>
        <View style={{alignItems: 'center', width: 200}}>
          <Text style={{
            fontSize: 16,
            marginTop: 10,
            color: 'white',
            textAlign: 'center',
            fontFamily: 'IRANSansMobile_Bold',
          }}>{this.props.text}</Text>
        </View>
        <Right>
          <Button
            onPress={() => {
              this.toggleModal(true);
            }}
            style={{paddingRight: 20, alignItem: 'Right'}} transparent>
            <Text style={{fontSize: 11, color: 'yellow', fontFamily: 'IRANSansMobile_Bold'}}>مشهد</Text>
          </Button>
        </Right>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#ede3f2',
    padding: 0,
  },
  modal: {
    borderWidth: 3,
    height: '60%',
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255,255,255,1)',
    marginTop: '30%',
    marginLeft: '10%',
    marginRight: '10%',
  },
  text: {
    color: '#3f2949',
    marginTop: 10,
    fontSize: 12,
    fontFamily: 'IRANSansMobile_Bold',
  },
});
