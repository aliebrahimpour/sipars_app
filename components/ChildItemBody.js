import React from 'react';
import {Image, ImageBackground, Picker, ScrollView, Text} from 'react-native';
import {Button, Item, Thumbnail} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,
  Header,
  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
  Tab, Tabs, TabHeading,
  ScrollableTab,

} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';
import WebApi from '../conection/WebApi';
import OrderStatic from '../staticClass/OrderStatic';

export default class ChildItemBody extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      item: null,
      child: [],
    };
    this.state.item = this.props.item;
    this.state.data = this.props.data;
  }

  // componentDidMount() {
  //   this.getChildItem(this.state.data, this.state.item.item);
  // }

  getChildItem(data, item) {
    return WebApi.filterItems(this.state.data, item.id, item.level + 1, true);
  }

  render() {
    const item = this.state.item.item;
    const childItems = this.getChildItem(this.state.data, item);
    return (
      <Button style={{height: 100, width: 100, marginLeft: 8}} onPress={() => {
        OrderStatic.service = item;

        this.props.navigation.navigate({
          routeName: 'chooseService',
          params: {childItems: childItems, data: this.state.data},
        });
      }}
              full transparent>

        <View style={{height: 100, width: 100, flexDirection: 'column'}}>
          <Card>
          <ImageBackground imageStyle={{opacity: 0.8, borderRadius: 8}}
                           source={{uri: 'http://api.sipars.ir/UserFiles/serviceImages/' + item.image}}
                           style={{height: '85%', width: 100}}>
          </ImageBackground>
          <View style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: '15%',
          }}>
            <Text style={{
              fontSize: 12,
              fontFamily: 'IRANSansMobile_Bold',
            }}> {item.title} </Text>
          </View>
          </Card>
        </View>
      </Button>
    );
  }
}
