import React from 'react';
import {Image, Picker, ScrollView, Text} from 'react-native';
import {Button, Item, Thumbnail} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,
  Header,
  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
  Tab, Tabs, TabHeading,
  ScrollableTab,

} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';


export default class exampleItam extends React.Component {
  render() {
    return (
        <View style={{flex: 1, alignItems: 'center', backgroundColor: 'rgb(226,224,225)'}}>
          <Card style={{width: '80%'}}>
            <CardItem>
              <Body>
              <Text style={{fontSize: 14, fontFamily: 'IRANSansMobile_Bold'}}>آموزش علوم اول دبستان</Text>

              <View style={{marginTop: 10, flexDirection: 'row'}}>
                <Text style={{color: '#49167f', fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>تاریخ درخواست:</Text>
                <Text style={{fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>75/000 هزار تومان</Text>
              </View>

              <View style={{marginTop: 5, flexDirection: 'row'}}>
                <Text style={{color: '#49167f', fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>تعرفه:</Text>
                <Text style={{fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>2 روز</Text>
              </View>
              </Body>
              <View>
                <Thumbnail source={require('../assets/images/bg.png')}/>
                <Text style={{fontSize: 12, fontFamily: 'IRANSansMobile_Bold'}}>علی ابراهیم پور</Text>
              </View>

            </CardItem>
            <View style={{flexDirection:"column",alignItems: 'center'}}>
              <Button style={{backgroundColor: '#d2b5eb'}} full>
                <Text style={{textAlign: "center", fontFamily: 'IRANSansMobile_Bold'}}>در دست بررسی</Text>
              </Button>
            </View>
          </Card>
        </View>



    );
  }

}
