import React from 'react';
import {FlatList, Image, ImageBackground, Picker, ScrollView, Text} from 'react-native';
import {Button, Item, Thumbnail} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,
  Header,
  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
  Tab, Tabs, TabHeading,
  ScrollableTab,

} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';
import ItemMyService from './ItemMyService';


export default class BodySelectService extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data,
      childItems: this.props.childItems,
    };
  }

  setInfoAndUpdate(childItems) {
    this.setState({childItems:childItems});
    this.forceUpdate();
  }


  render() {
    return (
      <FlatList
        numColumns={3}
        style={{
          width: '95%',
          height: '70%',
          marginLeft: 10,
          paddingLeft: 0,
          paddingRight: 0,
          marginRight: 0,
        }}
        data={this.state.childItems}
        selected={false}
        renderItem={(item) =>
          <ItemMyService key={item.item.id} navigation={this.props.navigation} setInfoAndUpdate={this.setInfoAndUpdate.bind(this)}
                         data={this.state.data} item={item}/>
        }
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }
}
