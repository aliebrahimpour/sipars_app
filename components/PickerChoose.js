import React, {Component} from 'react';
import {Picker} from 'react-native';

export default class PickerChoose extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currencies: [{'country': 'UK', 'currency': 'GBP', 'currencyLabel': 'Pound'}, {
        'country': 'EU',
        'currency': 'EUR',
        'currencyLabel': 'Euro',
      }, {'country': 'USA', 'currency': 'USD', 'currencyLabel': 'USD Dollar'}],
      currentLabel: 'Select your currency',
      currency: '',
    };
  }

  pickerChange(index) {
    this.state.currencies.map((v, i) => {
      if (index === i) {
        this.setState({
          currentLabel: this.state.currencies[index].currencyLabel,
          currency: this.state.currencies[index].currency,
        });
      }
    });
  }

  render() {
    // console.log(this.state.currencies);
    return (
      <Picker
        selectedValue={this.state.currency}
        onValueChange={(itemValue, itemIndex) => this.pickerChange(itemIndex)}>
        {this.state.currencies.map((v) => {
          return <Picker.Item label={v.currencyLabel} value={v.currency}/>;
        })}
      </Picker>
    );
  }
}


