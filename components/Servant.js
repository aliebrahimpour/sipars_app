import React from 'react';
import {FlatList, Image, ImageBackground, Picker, ScrollView, Text} from 'react-native';
import {Button, Item, Thumbnail} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,
  Header,
  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
  Tab, Tabs, TabHeading,
  ScrollableTab,

} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';
import ItemMyService from './ItemMyService';
import OrderStatic from '../staticClass/OrderStatic'

export default class servant extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
    };
  }

  renderOnPress(){
    OrderStatic.servant = this.state.item;
    this.props.navigation.navigate('rateSetting')
  }


  render() {
    const item = this.state.item.item;

    return (
      <Card style={{marginLeft: 5, marginRight: 5, width: '47%'}}>
        <CardItem>
          <Left>
            <Thumbnail source={{uri: 'http://sipars.ir/UserFiles/ProfilePicture/' + item.picture}}/>
            {/*<Thumbnail source={require('../assets/images/bg.png')}/>*/}
            <Body>
            <Text style={{fontSize: 10, fontFamily: 'IRANSansMobile_Bold'}}>{item.name + ' ' + item.family}</Text>
            </Body>
          </Left>
        </CardItem>
        {/*<CardItem cardBody>*/}
          {/*<Text numberOfLines={3}*/}
                {/*style={{marginLeft: 5, fontSize: 10, fontFamily: 'IRANSansMobile', marginRight: 5}}>{item.resume}</Text>*/}
        {/*</CardItem>*/}
        <CardItem>
          <Body>
          <Button style={{alignItem: 'center', backgroundColor: '#rgb(222,222,222)', borderRadius: 5}} rounded
                  full>
            <Text style={{fontSize: 12, fontFamily: 'IRANSansMobile_Bold'}}>مشاهده
              رزومه</Text>
          </Button>
          </Body>
        </CardItem>
        <CardItem>
          <Left>
            <Button
              onPress={() =>this.renderOnPress()}
              style={{width: '90%', borderRadius: 5, justifyContent: 'center'}} success>
              <Text style={{fontSize: 11, color: 'white', fontFamily: 'IRANSansMobile_Bold'}}>انتخاب
                خدمتیار</Text>
            </Button>
          </Left>
          <Button style={{width: '30%', borderRadius: 5, alignItems: 'center', justifyContent: 'center'}}
                  danger>
            <Text style={{fontSize: 11, fontFamily: 'IRANSansMobile_Bold'}}>نظرات</Text>
          </Button>
        </CardItem>
      </Card>
    );
  }
}
