import React from 'react';
import {Image, ImageBackground, Picker, ScrollView, Text} from 'react-native';
import {Button, Item, Thumbnail} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Icon2 from 'react-native-vector-icons/Fontisto';
import {
  Footer,
  CardItem,
  Container,
  FooterTab,
  View,
  Header,
  Card,
  Radio,
  Content,
  List,
  ListItem,
  Textarea,
  CheckBox,
  Body,
  Left,
  Right,
  Tab, Tabs, TabHeading,
  ScrollableTab,

} from 'native-base';
import {Col, Grid} from 'react-native-easy-grid';
import BodySelectService from './BodySelectService';
import WebApi from '../conection/WebApi';
import OrderStatic from '../staticClass/OrderStatic';

export default class itemMyService extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      item: this.props.item,
      parentArray: [],
    };
  }


  renderBody(item) {
    // console.log(OrderStatic.serviceSelect);
    const childItems = this.getChildItem(this.props.data, item);
    if (childItems.length > 0) {
      return this.props.setInfoAndUpdate(childItems);
    } else {
      OrderStatic.serviceSelect = item
      return this.props.navigation.navigate('chooseType');
    }
  }

  getChildItem(data, item) {
    return WebApi.filterItems(data, item.id, item.level + 1, true);
  }

  render() {
    const item = this.state.item.item;

    return (
      <ImageBackground imageStyle={{opacity: 0.2, borderRadius: 8}}
                       source={{uri: 'http://api.sipars.ir/UserFiles/serviceImages/' + item.image}}
                       style={{
                         height: 100, width: 100, marginTop: 10,
                         marginLeft: 5,
                         marginRight: 3,
                       }}>
        <Button transparent
                onPress={() => this.renderBody(item)}
          // onPress={() => this.props.navigation.navigate('chooseType')}

                style={{
                  alignItems: 'center',
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  flexDirection: 'column',
                }} full>
          <Text
            style={{fontSize: 16, textAlign: 'center', fontFamily: 'IRANSansMobile_Bold'}}>{item.title}</Text>
        </Button>
      </ImageBackground>
    );
  }
}
