import React, {Component} from 'react';
import {Text, View, TouchableOpacity, StyleSheet, FlatList} from 'react-native';
import ChildItemBody from '../all pages/pagesApp/Home/home';
import {List} from 'native-base';

export default class RadioButtons extends Component {
  state = {
    value: 0,
  };

  renderItem(item, value) {
    return (
      <View key={item.id} style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.circle}
          onPress={() => {
            this.setState({
              value: item.id,
            });
          }}
        >
          {value === item.id && <View style={styles.checkedCircle}/>}
          <Text style={{fontFamily: 'IRANSansMobile_Bold'}}>{item.name}</Text>

        </TouchableOpacity>
      </View>
    );
  }


  render() {
    const {options} = this.props;
    const value = this.state.value;

    return (
      <FlatList
        style={{
          width: '100%',
          height: '100%',
        }}
        data={options}
        selected={false}
        numColumns={3}
        renderItem={(item) =>
          this.renderItem(item.item, value)
        }
        keyExtractor={(item, index) => index.toString()}
      />
    );
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
    marginRight: 5,
    marginLeft: 10,
    marginTop: 5,
  },

  circle: {
    height: 40,
    width: 80,
    flexDirection: 'row',
    borderRadius: 10,
    borderWidth: 2,
    borderColor: '#ACACAC',
    alignItems: 'center',
    justifyContent: 'center',
  },

  checkedCircle: {
    width: 14,
    height: 14,
    borderRadius: 7,
    backgroundColor: '#794F9B',
  },
});
